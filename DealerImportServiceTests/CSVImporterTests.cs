namespace DealerImportServiceTests
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.VisualStudio.TestTools.UnitTesting;  
    using System.IO;

    using DealerImportService;
    using System;
    using System.Linq;
    using System.Text;

    [TestClass]
    public class CSVImporterTests
    {
        private InventoryContext context;
        private CSVImporter importer;
        private TextWriter failedLinesStream;
        private StringBuilder failedLines;

        public CSVImporterTests()
        {

        }
        
        [TestInitialize()]
        public void Initalize()
        {
            context = InMemoryDB();
            importer = new CSVImporter();
            failedLines = new StringBuilder();
            failedLinesStream = new StringWriter(failedLines);
        }

        [TestCleanup()]
        public void Cleanup()
        {
            context.Database.EnsureDeleted();
            context.Dispose();
            context = null;

            importer = null;

            failedLinesStream.Dispose();
            failedLinesStream = null;

            failedLines = null;
        }

        [TestMethod]
        public void ValidVehiclesWrittenToDB()
        {
            var csv = CreateCSVStream(
                "1001,\"Strathcom Motors\",\"New\",\"A123\",\"4VHJCMPF3XN811885\", 2018,\"Ford\",\"F-150\",\"Crew Cab Pickup\",\"XLT\", 4,\"Shadow Black\",\"Medium Earth Gray\", 6,\"3.5 L\",\"10-Speed Auto-Shift Manual w/OD\", 50, 53922, 53922,\"FALSE\",\"2017-09-04\",\"Come check out this beautiful shadow black F150 today!\",\"V\",\"Gasoline\",\"4WD\",\"Black\",, 6",
                "1022,\"Fine Cars Inc\",\"New\",\"Z187\",\"1FVX3EDB6SP668351\", 2018,\"Jaguar\",\"XE\",\"4dr Car\",\"Premium\", 4,\"Fuji White\",\"Ebony Black\", 4,\"2.0 L\",\"8-Speed Automatic w/OD\", 25, 53565, 48250,\"FALSE\",\"2017-07-07\",\".\",\"I\",\"Gasoline\",\"AWD\",\"Grey\",, 5"
                );

            var results = importer.Import(csv, context, failedLinesStream);

            Assert.AreEqual(2, results.ImportedCount, "Wrong number of vehicles imported");
            Assert.AreEqual(0, results.Failed.Count, "Wrong number of vehicles failed to import");
            Assert.AreEqual(2, context.Inventory.CountAsync().Result, "Wrong number of vehicles written to db");

            // Not checking all of the fields due to time. TODO: One test should check every field.
            var strathcomMotors = context.Inventory.Where(vehicle => vehicle.DealerId == 1001).FirstOrDefault();
            Assert.IsNotNull(strathcomMotors, "Couldn't find the first vehicle written");
            Assert.AreEqual("Strathcom Motors", strathcomMotors.DealerName);
            Assert.AreEqual("F-150", strathcomMotors.Model);
            Assert.AreEqual(3, 5, strathcomMotors.Displacement);
            Assert.AreEqual(null, strathcomMotors.InteriorColourGeneric);
            Assert.AreEqual("Gasoline", strathcomMotors.FuelType);

            // And just check that the other one got the right dealer
            var fineCarsInc = context.Inventory.Where(vehicle => vehicle.DealerId == 1022).FirstOrDefault();
            Assert.IsNotNull(fineCarsInc, "Couldn't find the first vehicle written");
            Assert.AreEqual("Fine Cars Inc", fineCarsInc.DealerName);
        }

        [TestMethod]
        public void InvalidDisplacementIsCaught()
        {
            var invalidDisplacementLine = "1022,\"Invalid Displacement Inc\",\"New\",\"Z187\",\"1FVX3EDB6SP668351\", 2018,\"Jaguar\",\"XE\",\"4dr Car\",\"Premium\", 4,\"Fuji White\",\"Ebony Black\", 4,\"2.0.0 L\",\"8-Speed Automatic w/OD\", 25, 53565, 48250,\"FALSE\",\"2017-07-07\",\".\",\"I\",\"Gasoline\",\"AWD\",\"Grey\",, 5";

            var csv = CreateCSVStream(
                "1001,\"Strathcom Motors\",\"New\",\"A123\",\"4VHJCMPF3XN811885\", 2018,\"Ford\",\"F-150\",\"Crew Cab Pickup\",\"XLT\", 4,\"Shadow Black\",\"Medium Earth Gray\", 6,\"3.5 L\",\"10-Speed Auto-Shift Manual w/OD\", 50, 53922, 53922,\"FALSE\",\"2017-09-04\",\"Come check out this beautiful shadow black F150 today!\",\"V\",\"Gasoline\",\"4WD\",\"Black\",, 6",
                invalidDisplacementLine
                );

            var results = importer.Import(csv, context, failedLinesStream);

            Assert.AreEqual(1, results.ImportedCount, "Wrong number of vehicles imported");
            Assert.AreEqual(1, results.Failed.Count, "Wrong number of vehicles failed to import");

            var failedLinesCSV = failedLines.ToString();
            Assert.IsTrue(failedLinesCSV.Contains(invalidDisplacementLine), "The failing line wasn't written to the error file");
        }

        private StringReader CreateCSVStream(params string[] vehicles)
        {
            var combinedVehicles = string.Join("\n", vehicles);

            //TODO: There's probably a way to do this without escaping the " but I can't find it at the moment. If you find it you can make this and the other tests much nicer.            
            var header = "\"DealerID\",\"DealerName\",\"Type\",\"Stock\",\"VIN\",\"Year\",\"Make\",\"Model\",\"Body\",\"Trim\",\"Doors\",\"ExtColor\",\"IntColor\",\"EngCylinders\",\"EngDisplacement\",\"Transmission\",\"Odometer\",\"Price\",\"MSRP\",\"Certified\",\"DateInStock\",\"Description\",\"EngType\",\"EngFuel\",\"Drivetrain\",\"ExtColorGeneric\",\"IntColorGeneric\",\"PassengerCount\"";

            //TODO: It's awkward that we're joining the header seperately than the vehicles. The problem is that join takes params which doesn't expand the string[]. 
            var vehicleWithHeader = string.Join("\n", header, combinedVehicles);

            return new StringReader(vehicleWithHeader); 
        }

        private InventoryContext InMemoryDB()
        {
            var optionsBuilder = new DbContextOptionsBuilder<InventoryContext>();
            optionsBuilder.UseInMemoryDatabase("TestInventory");
            return new InventoryContext(optionsBuilder.Options);
        }
    }
}
