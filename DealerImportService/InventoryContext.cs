﻿namespace DealerImportService
{
    using Microsoft.EntityFrameworkCore;

    //TODO: InventoryContext is a bad name but I don't want to call it StrathcomContext. What else could be in this database?
    public class InventoryContext : DbContext
    {
        public InventoryContext(DbContextOptions<InventoryContext> options) : base(options)
        {

        }

        public virtual DbSet<Inventory> Inventory { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Inventory>()
                .HasKey(inventory => new { inventory.Id });
        }
    }
}
