﻿namespace DealerImportService
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using CsvHelper;

    public class CSVImporter
    {
        public ImportResult Import(TextReader csvReader, InventoryContext db, TextWriter invalidLinesWriter)
        {
            var results = new ImportResult();

            using (var csv = new CsvReader(csvReader, CultureInfo.InvariantCulture))
            {
                //Save empty strings as null
                csv.Configuration.TypeConverterOptionsCache.GetOptions<string>().NullValues.Add("");

                //Write failed lines to the invalid lines stream
                csv.Configuration.ReadingExceptionOccurred = (CsvHelperException exception) =>
                {
                    HandleFailedLine(results, exception.ToString(), exception.ReadingContext, invalidLinesWriter);
                    return false; //Returning false means ignore the error
                };

                var inventory = csv.GetRecords<Inventory>();
                foreach (var vehicle in inventory)
                {
                    try
                    {
                        vehicle.CreatedTime = DateTime.Now;
                        vehicle.LastModifiedBy = "IMPORT";
                        vehicle.LastModifiedTime = vehicle.CreatedTime;
                        db.Inventory.Add(vehicle);
                        db.SaveChanges();
                        results.ImportedCount++;
                    }
                    catch(Exception e)
                    {
                        HandleFailedLine(results, e.ToString(), csv.Context, invalidLinesWriter);
                    }                    
                }
            }

            return results;
        }

        public void HandleFailedLine(ImportResult results, string errorReason, ReadingContext csvContext, TextWriter invalidLinesWriter)
        {
            results.Failed.Add(new ImportResult.FailedLine()
            {
                Row = csvContext.Row,
                Reason = errorReason,
            });

            //If this is the first record that failed write the header record to the error file
            if (results.Failed.Count == 1)
            {
                //It's awkward that we need to rebuild the header record because it may not be identical afterward, but unfortunately CSVHelper doesn't expose the raw header.
                invalidLinesWriter.WriteLine(string.Join(",", csvContext.HeaderRecord));
            }

            //Write instead of WriteLine because CSV lines tend to end in a newline
            invalidLinesWriter.Write(csvContext.RawRecord);
        }
    }

    public class ImportResult
    {
        public int ImportedCount { get; set; }
        public List<FailedLine> Failed;

        public ImportResult()
        {
            Failed = new List<FailedLine>();
        }

        public class FailedLine
        {
            public int Row { get; set; }
            public string Reason { get; set; }
        }
    }
}
