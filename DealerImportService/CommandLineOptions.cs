﻿namespace DealerImportService
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using CommandLine;

   public class CommandLineOptions
    {
        [Option('d', "database", Required = false, HelpText = "SQLite DB to add inventory to", Default = "dealer_import.db")]
        public string DatabasePath { get; set; }

        [Option('c', "csv", Required = false, HelpText = "CSV file to read inventory from", Default = "dealer_import.csv")]
        public string CSVPath { get; set; }

        [Option('e', "errors", Required = false, HelpText = "CSV file to write error lines to", Default = "import_errors.csv")]
        public string ErrorPath { get; set; }
    }
}
