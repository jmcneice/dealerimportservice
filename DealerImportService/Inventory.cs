﻿namespace DealerImportService
{
    using System;
    using CsvHelper;
    using CsvHelper.Configuration;
    using CsvHelper.Configuration.Attributes;
    using System.Text.RegularExpressions;
    using CsvHelper.TypeConversion;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.ComponentModel.DataAnnotations;

    public class Inventory
    {
        [Name("DealerID")]
        [Column("d_id")]
        public int DealerId{ get; set; }

        [Name("DealerName")]
        [Column("d_name")]
        public string DealerName{ get; set; }

        [Name("Type")]
        [Column("stock_type")]
        [TypeConverter(typeof(StringToUppercase))]
        public string StockType{ get; set; }

        [Name("Stock")]
        [Column("stock_id")]
        public string StockId{ get; set; }

        [Name("VIN")]
        [Column("vin")]
        public string VIN{ get; set; }
        [Name("Year")]
        [Column("year")]
        public int Year{ get; set; }

        [Name("Make")]
        [Column("make")]
        public string Make{ get; set; }

        [Name("Model")]
        [Column("model")]
        public string Model{ get; set; }

        [Name("Body")]
        [Column("body_style")]
        public string Body{ get; set; }

        [Name("Trim")]
        public string Trim{ get; set; }

        [Name("Doors")]
        public int Doors{ get; set; } 

        [Name("ExtColor")]
        [Column("exterior_colour")]
        public string ExteriorColour{ get; set; }

        [Name("IntColor")]
        [Column("interior_colour")]
        public string InteriorColour { get; set; }

        [Name("ExtColorGeneric")]
        [Column("exterior_colour_generic")]
        public string ExteriorColourGeneric{ get; set; }

        [Name("IntColorGeneric")]
        [Column("interior_colour_generic")]
        public string InteriorColourGeneric{ get; set; }

        [Name("EngType")]
        [Column("configuration")]
        public string Configuration{ get; set; }
        [Name("EngCylinders")]
        [Column("cylinders")]
        public int Cylinders{ get; set; }

        [Name("EngDisplacement")]
        [TypeConverter(typeof(DisplacementStringToDouble))] // In the CSV this is provided with the unit, "L", so we use a type converter to get the double
        [Column("displacement")]
        public double Displacement { get; set; }

        [Name("EngFuel")]
        [Column("fuel_type")]
        public string FuelType{ get; set; }

        [Name("Transmission")]
        [Column("transmission_description")]
        public string TramissionDescription{ get; set; }
        [Name("Drivetrain")]
        public string Drivetrain{ get; set; }

        [Name("Odometer")]
        public int Odometer{ get; set; } 

        [Name("Price")]
        public double Price{ get; set; } 

        [Name("MSRP")]
        public double MSRP{ get; set; } 

        [Name("Description")]
        public string Description{ get; set; }

        [Name("PassengerCount")]
        [Column("passengers")]
        public int Passengers{ get; set; } 

        [Name("Certified")]
        [NotMapped] //This is in the CSV but not the DB.
        public bool? Certified{ get; set; }

        [Name("DateInStock")]
        [NotMapped] //This is in the CSV but not the DB.
        public DateTime? DateInStock{ get; set; }

        [Ignore()]
        [Column("v_id")]
        [Key]
        public int Id{ get; set; }

        [Ignore()] //Doesn't exist in CSV
        [Column("created_time")]
        public DateTime CreatedTime{ get; set; }

        [Ignore()]
        [Column("last_modified_by")]
        public string LastModifiedBy{ get; set; }

        [Ignore()]
        [Column("last_modified_time")]
        public DateTime LastModifiedTime{ get; set; }

        [Ignore()]
        [Column("transmission_speeds")]
        public int TransmissionSpeeds{ get; set; } //This is in the DB but not the CSV.

        [Ignore()]
        [Column("transmission_type")]
        public string TransmissionType{ get; set; } //This is in the DB but not the CSV.
    }

    class DisplacementStringToDouble : DefaultTypeConverter
    {
        public override object ConvertFromString(string text, IReaderRow row, MemberMapData memberMapData)
        {
            try
            {
                var textWithoutNonNumericCharacters = Regex.Replace(text, "[^0-9.]", "");
                var displacement = Convert.ToDouble(textWithoutNonNumericCharacters);
                return displacement;
            }
            catch
            {
                throw new InvalidOperationException("Unable to convert Displacement to a double. Displacement must contain a positive double after all nonnumeric characters are removed.");
            }
        }

        public override string ConvertToString(object value, IWriterRow row, MemberMapData memberMapData)
        {
            return value.ToString() + " L";
        }
    }

    class StringToUppercase : DefaultTypeConverter
    {
        public override object ConvertFromString(string text, IReaderRow row, MemberMapData memberMapData)
        {
            return text.ToUpper();
        }

        public override string ConvertToString(object value, IWriterRow row, MemberMapData memberMapData)
        {
            return value.ToString();
        }
    }
}
