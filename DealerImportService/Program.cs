﻿namespace DealerImportService
{
    using System;
    using System.IO;
    using Microsoft.EntityFrameworkCore;
    using CommandLine;

    class Program
    {
        static void Main(string[] args)
        {
            var commandLineOptions = ParseArguments(args);
            using (var db = OpenDB(commandLineOptions.DatabasePath))
            {
                using (var csv = new StreamReader(commandLineOptions.CSVPath))
                {
                    //TODO: We're appending to avoid stomping old errors, but we're also writing the header record each time. Maybe check if the file already exists and don't write the header?
                    using (var errors = new StreamWriter(commandLineOptions.ErrorPath, true))
                    {                        
                        var importer = new CSVImporter();
                        var results = importer.Import(csv, db, errors);
                        Console.WriteLine($"Imported {results.ImportedCount} vehicles");
                        Console.WriteLine($"Failed to import {results.Failed.Count} vehicles");
                        foreach(var failedRows in results.Failed)
                        {
                            Console.WriteLine($"Row {failedRows.Row}: {failedRows.Reason}");
                        }
                    }

                }
            }
        }

        static CommandLineOptions ParseArguments(string[] args)
        {
            CommandLineOptions commandLineOptions = null;
            var commandLineParsingResult = Parser.Default.ParseArguments<CommandLineOptions>(args)
                    .WithParsed(options => commandLineOptions = options);
            if (commandLineOptions == null)
            {
                throw new Exception("Invalid arguments");
            }

            return commandLineOptions;
        }

        static InventoryContext OpenDB(string databasePath)
        {
            var optionsBuilder = new DbContextOptionsBuilder<InventoryContext>();
            optionsBuilder.UseSqlite($"Data Source={databasePath}");
            return new InventoryContext(optionsBuilder.Options);
        }
    }
}
